-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mer. 08 nov. 2017 à 17:46
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `id1475067_bddlivredor`
--
CREATE DATABASE IF NOT EXISTS `id1475067_bddlivredor` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `id1475067_bddlivredor`;

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `pseudo` varchar(144) NOT NULL,
  `password` varchar(144) NOT NULL,
  `dateCreation` datetime NOT NULL,
  `description` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `member`
--

INSERT INTO `member` (`id`, `sexe`, `pseudo`, `password`, `dateCreation`, `description`) VALUES
(9, 'M', 'ken', '$2y$10$EdCi96QusrxHghPe1oC3N.qL8Hj8LRDKN8ezob42QCRJwUo1bk5vu', '2017-06-22 02:38:08', 'Testeur officiel du site.'),
(19, 'M', 'jean_rochefort', '$2y$10$cjrd350pD4i803vWZ.I.LOHFoqnIL4U3m0bMphmDZvY.gxJU7nBXO', '2017-11-08 18:14:44', 'NuLl'),
(20, 'F', 'patricia', '$2y$10$ZMpicAPWU2XFVMgHau3bQebsYG8AuzgT8uSS3D1NE/TDlHrg3OWIa', '2017-11-08 18:21:17', 'Patricia Evra');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `member` varchar(140) NOT NULL,
  `hour` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id`, `member`, `hour`, `text`) VALUES
(27, 'ken', '2017-06-29 21:26:56', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standar'),
(29, 'ken', '2017-07-05 23:48:08', 'ceci est un test mobile le css ne s\'est pas encore actualisé je ne sais pas encore pourquoi'),
(41, 'ken', '2017-08-17 21:33:18', 'test mobile 100% réussi '),
(45, 'jean_rochefort', '2017-08-30 03:18:23', 'Signé noble bourgeois'),
(46, 'ken', '2017-11-05 16:19:08', 'Je remercie de tout cœur ma prof d\'allemand Madame Putch(La Fontaine des Eaux).\nJe la connais depuis peu mais elle est déjà dans mon cœur. '),
(47, 'patricia', '2017-11-08 07:22:10', 'J\'adore votre site il est vraiment super chouettos!');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;