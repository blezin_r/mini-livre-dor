<?php

include 'Application/bdd_connexion.php';

if(!empty($_POST)){
     $sexe = $_POST['sexe'];
     $pseudo = htmlspecialchars(strtolower($_POST['pseudo']));
     $password = password_hash($_POST['password'] , PASSWORD_BCRYPT);
     
     $req = $pdo->prepare('SELECT `pseudo` FROM `member` WHERE pseudo = ?');
     $req->execute(array($pseudo));
     $verif = $req->fetch();
     
     if($verif != false){
          
          session_start();
          
          $_SESSION = array('psd' => 1);
          
          header('Location: index.php');
          exit;
     } else {
     
          if ($_POST['description'] == ''){
               $description = 'NuLl';
          } else {
               $description = htmlspecialchars($_POST['description']);
          }
          $req = $pdo->prepare('INSERT INTO `member`(`sexe`,
                                                       `pseudo`, 
                                                       `password`, 
                                                       `dateCreation`, `description`)
                                        VALUES(?,?,?,NOW(),?)');
          $req->execute(array($sexe,$pseudo,$password, $description));
          
          session_start();
          
          $_SESSION = array('felicitation' => 1);
          header("Location: index.php");
          exit;
     }
} else {
     header("Location: index.php");
     exit;
}