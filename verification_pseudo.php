<?php

include('Application/bdd_connexion.php');


//Verification si le pseudo existe dans la BDD
if(!empty($_POST)){
     $req = $pdo->prepare('SELECT `pseudo` FROM `member` WHERE pseudo = ?');
     $req->execute(array($_POST['pseudo']));
     $verif = $req->fetch();
     
     echo $verif != false ? 1 : 0;
} else {
     header('Location: index.php');
     exit;
}