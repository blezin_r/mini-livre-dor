<?php

include('Application/bdd_connexion.php');

//Verifie si les 6 derniers caracteres du mot de passe crypté envoyé en POST (input en type=hidden) correspondent à ceux contenue dans la BDD
//Si tout est OK le message est ajouté à la BDD sinon une erreur est renvoyé
if(!empty($_POST)){
     $member = $_POST['member'];
     $message = substr(htmlspecialchars($_POST['message']), 0, 150);
     $mdpVerif = $_POST['mdpVerif'];
     
     $requete = $pdo->prepare('SELECT * FROM `member` WHERE pseudo = ?');
     $requete->execute(array($member));
     $verif = $requete->fetch();
     $mdpBdd = substr($verif['password'], -6);
     
     if($mdpBdd == $mdpVerif){
          $req = $pdo->prepare('INSERT INTO `message` (`member` ,
                                                       `hour` ,
                                                       `text` )
                              VALUES ( ?, NOW(),?)');
          $req->execute(array($member, $message));
     } else {
          echo 'erreur';
     }
} else {
     header('Location: index.php');
     exit;
}