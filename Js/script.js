var buttonSubscribe = $('#subscribe');
var buttonLogin = $('#login');


// Charger le formulaire pour s'inscrire
function onClickSubscribe(event){
     event.preventDefault();
     
     onWaitLoadTheLoader();
     $('#laZone').load('subscribe.php');
     $('header #login').show();
     $('header #subscribe').hide();
}

// Vérifier si le pseudo contient des caracteres spéciaux ou si il existe dèjà dans la BDD (formulaire d'inscription)
function onBlurPseudo(){
     var pseudo = $('[name=pseudo]');

     if(!/\W/i.test(pseudo.val())) {
          $.ajax({
               url: 'verification_pseudo.php',
               type: 'POST',
               data: 'pseudo=' + pseudo.val().toLowerCase(),
               success: function(data){
                    if(data == 1){
                         alert('Ce pseudo existe déjà');
                         pseudo.addClass('red');
                    } else {
                         pseudo.removeClass('red');
                    }
               }
          });
     } else {
         alert('Le pseudo "' +  pseudo.val().toLowerCase()  + '" contient un ou des caractères spéciaux');
         pseudo.val('');
     }
}

// Test pour savoir si le mot de passe; fait bien plus de 8 caractères, soit identique avec le mot de passe de verification
function onCheckPassword(passwordOne , passwordTwo){
     return (passwordOne == passwordTwo) ? (passwordOne.length < 8) ? 1 : 2 : 3;
}

// Verification des données entrées dans le formulaire avant avant meme de les envoyer au PHP
function onClickSubmitSubscribe(event){
     var resultatVerif = onCheckPassword($('[name=password]').val(), $('[name=passwordConfirmation]').val());
     
     if($('[name=pseudo]').attr('class')){
          alert('Veuillez changer de pseudo celui ci est deja utilisé');
          return false;
     }
     
     switch(resultatVerif){
          case 1: 
               alert('Le mot de passe doit faire au moins 8 caracteres');
               $('[name=password]').val('');
               $('[name=password]').css('borderColor', 'red');
               $('[name=passwordConfirmation]').val('');
               return false;
               break;
          case 2:
               break;
          case 3:
               alert('Les mots de passe ne sont pas identiques');
               $('[name=password]').val('');
               $('[name=passwordConfirmation]').val('');
               return false;
               break;
     }
}


// Charger le formulaire pour se connecter
function onClickLogin(event){
     event.preventDefault();
     
     onWaitLoadTheLoader();
     $('#laZone').load('login.php');
     $('header #login').hide();
     $('header #subscribe').show();
}

//Verification du pseudo et du mot de passe entrer dans le formulaire envoyer en POST à la page connexion.php pour traitement
//Si connexion.php retourne 0 apres traitement, alors un message d'erreur apparaitra
//Sinon le livre d'or apparaitra
function onClickButtonSubmitLoginForm(event){
     var pseudo = $('[name=pseudo]').val().toLowerCase();
     var password = $('[name=password]').val();
     onWaitLoadTheLoader();


     $.ajax({
          url: 'connexion.php',
          type: 'POST',
          data: 'pseudo=' + pseudo + '&mdp=' + password,
          success: function(data){
               if(data == 0){
                    $('#laZone').html("<div id='errorlog'>\
                                        <p>Erreur de saisie du login ou du mot de passe.</p>\
                                        <p>Les informations saisies sont incorrectes.</p>\
                                        <p>Merci de bien vouloir vérifier les informations saisies.</p>\
                                        </div>");
                                        $('header #login').show();
               } else {
                    $('#laZone').html(data);
                    $('header nav ul').replaceWith('<ul>\
                                                       <li>\
                                                            <a id="disconnect" href="#">Deconnection</a>\
                                                       </li>\
                                                  </ul>');
                    }
               }
     });
     return false;
}


//Ajout du message dans la BDD + rafraichissement de la page
function onClickButtonSubmitSendMessage(){
     var member = $('#laZone #sendMessage [name=member]').val();
     var message = $("#laZone #sendMessage [name=message]").val();
     var mdpVerif = $('#laZone #sendMessage [name=mdpVerif]').val();
     
     onWaitLoadTheLoader();
     $.ajax({
          url: 'add_message.php',
          type: 'POST',
          data: 'member=' + member + '&message=' + message + '&mdpVerif=' + mdpVerif,
          success: function(data){
               if(data == 'erreur'){
                    $('#laZone').html(data);
               } else {
                    $('#laZone').html('Votre message à bien été envoyer<br>Merci');
                    $.ajax({
                         url: 'connexion.php',
                         type: 'POST',
                         data: 'refresh=1' + '&member=' + member + '&mdpVerif=' + mdpVerif,
                         success: function(data){
                              $('#laZone').html(data);
                         }
                    });
               }
          }
     });
     return false;
}


//Retourner sur l'index en cliquant sur le boutton retour
function onClickButtonReturn(event){
     onWaitLoadTheLoader();
     $('#laZone').load('layout.phtml');
     return false;
}

//Retourner sur l'index en rechargeant le site en cliquant sur le button de deconnection
function onClickButtonDisconnect(event){
     location.reload();
     return false;
}


function onClickUpdateDescription(event){
     $('#laZone #leMembre #theDescription').replaceWith('<textarea placeholder="Limité à 150 caractères" maxlength="150"></textarea>');
     $('#laZone #leMembre #updateDescription').replaceWith('<a id="updateDescriptionSubmit" href="#">Ok</a>');
     
     return false;
}


//Changer la description
function onClickUpdateDescriptionSubmit(event){
     var member = $('#laZone #sendMessage [name=member]').val();
     var mdpVerif = $('#laZone #sendMessage [name=mdpVerif]').val();
     var description = $('#laZone #leMembre textarea').val();
     
     $.ajax({
          url: 'update_description.php',
               type: 'POST',
               data: 'member=' + member + '&mdpVerif=' + mdpVerif + '&description=' + description,
               success: function(data){
                    onWaitLoadTheLoader();
                    $.ajax({
                         url: 'connexion.php',
                         type: 'POST',
                         data: 'refresh=1' + '&member=' + member + '&mdpVerif=' + mdpVerif,
                         success: function(data){
                              $('#laZone').html(data);
                         }
                    });
               }
     });
     return false;
}


function onClickButtonDeleteAccount(event){
     var member = $('#laZone #sendMessage [name=member]').val();
     var mdpVerif = $('#laZone #sendMessage [name=mdpVerif]').val();
     var verif = confirm('Vous etes sur le point de supprimer votre compte Ok ?');
     
     if(verif){
          alert('Votre compte à été supprimé maintenant vous allez etre rediriger sur la page d\'accueuil');
          
          onWaitLoadTheLoader();
          $.ajax({
               url: 'delete_account.php',
               type: 'POST',
               data: 'member=' + member + '&mdpVerif=' + mdpVerif,
               success: function(data){
                    $('body').html(data);
                    location.reload();
               }
          });
     }
     return false;
}

//Rafraichissement de la page lorsque l'on appuie sur le button avec conservation du message déjà tapé
function onClickButtonRefresh(event){
     var member = $('#laZone #sendMessage [name=member]').val();
     var message = $("#laZone #sendMessage [name=message]").val();
     var mdpVerif = $('#laZone #sendMessage [name=mdpVerif]').val();

     onWaitLoadTheLoader();
     $.ajax({
          url: 'connexion.php',
          type: 'POST',
          data: 'refresh=1' + '&member=' + member + '&mdpVerif=' + mdpVerif,
          success: function(data){
               $('#laZone').html(data);
               $('#laZone #sendMessage textarea').val(message);
          }
     });
     return false;
}


function onWaitLoadTheLoader(){
     $('#laZone').html('<img id="loader" src="Images/loader.gif" alt="Loader"/>');
}

(function(){
     buttonSubscribe.on('click', onClickSubscribe);
     buttonLogin.on('click', onClickLogin);
     $(document).on('blur', 'form#subscribe [name=pseudo]' , onBlurPseudo);
     $(document).on('click', 'form#subscribe [type=submit]', onClickSubmitSubscribe);
     $(document).on('click', 'form#loginForm input[type=submit]', onClickButtonSubmitLoginForm);
     $(document).on('click', '#laZone #buttonReturn' , onClickButtonReturn);
     $(document).on('click', 'header nav #disconnect', onClickButtonDisconnect);
     $(document).on('click', '#laZone #leMembre #deleteAccount', onClickButtonDeleteAccount);
     $(document).on('click', '#laZone #leMembre #updateDescription', onClickUpdateDescription);
     $(document).on('click', '#laZone #leMembre #updateDescriptionSubmit', onClickUpdateDescriptionSubmit);
     $(document).on('click', '#laZone #sendMessage input[type=submit]', onClickButtonSubmitSendMessage);
     $(document).on('click', '#laZone #sendMessage [name=refresh]', onClickButtonRefresh);
     // Supprimer la derniere div contenant la pub "Power By 000webhost"
     $('div').last().remove();
})();