<?php

include('Application/bdd_connexion.php');

if(!empty($_POST)){
     
     $pseudo = $_POST['member'];
     $mdpVerif = $_POST['mdpVerif'];
     
     $requete = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
     $requete->execute(array($pseudo));
     $row = $requete->fetch();
     
     $mdpBdd = substr($row['password'], -6);
     
     if($mdpBdd == $mdpVerif){
          $req = $pdo->prepare('DELETE FROM `minichat`.`member` WHERE pseudo = ?');
          $req->execute(array($pseudo));
          echo 'Votre compte à été supprimé';
     } else {
          echo 'Erreur';
     }
} else {
     header('Location: index.php');
     exit;
}