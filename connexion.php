<?php

include('Application/bdd_connexion.php');
include('utilities.php');


//
if(!empty($_POST)){
    
    //Cette partie du script est éxécuté lorsque l'on a envoyé un message la page se 'rafraichi'
    if($_POST['refresh']){
        
        $pseudo = $_POST['member'];
        $mdpVerif = $_POST['mdpVerif'];
        
        $requete = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
        $requete->execute(array($pseudo));
        $row = $requete->fetch();
        
        //6 derniers caracteres du mot de passe crypté dans la BDD
        $mdpBdd = substr($row['password'], -6);
        
        //Verification si les 6 derniers caracteres crypté du mot de passe envoyé en POST (dans un input type=hidden) correspondent
        if($mdpBdd == $mdpVerif){
            $req = $pdo->query('SELECT * FROM `message` ORDER BY `message`.`hour` DESC LIMIT 0, 100');
            $msg = $req->fetchAll();
            
            $req = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
            $req->execute(array($pseudo));
            $afficheMembre = $req->fetch();
            
            include('connexion.phtml');
        } else {
            echo 'erreur lors du rafraichissement';
        }
    } else {
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $password = $_POST['mdp'];
        
        $requete = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
        $requete->execute(array($pseudo));
        $row = $requete->fetch();
        
        $result = password_verify($password, $row['password']);
        
        if($result){
            $req = $pdo->query('SELECT * FROM `message` ORDER BY `message`.`hour` DESC LIMIT 0, 100');
            $msg = $req->fetchAll();
            
            $req = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
            $req->execute(array($pseudo));
            $afficheMembre = $req->fetch();
            
            include('connexion.phtml');
        } else {
            echo 0;
        }
    }
} else {
    header('Location: index.php');
    exit;
}