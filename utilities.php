<?php

function formatDate($date){
     $jours = ['Lundi','Mardi', 'Mercredi', 
               'Jeudi','Vendredi','Samedi','Dimanche'];
     
     $mois = ['Decembre','Janvier','Fevrier','Mars','Avril',
               'Mai','Juin','Juillet','Aout',
               'Septembre','Octobre','Novembre'];
    
    $mounth = intval(date_format(date_create($date), 'm'));
    $jour = intval(date_format(date_create($date), 'j'));
    $year = intval(date_format(date_create($date), 'Y'));
    
    $hour = intval(date_format(date_create($date), 'H'));
    $hour = $hour >= 0 && $hour < 10 ? '0' . $hour : $hour;
    $minuts = intval(date_format(date_create($date), 'i'));
    $secondes = intval(date_format(date_create($date), 's'));
    
    $tab = ['moisEcrit' => $mois[$mounth], 'jour' => $jour, 'annee' => $year, 'heure' => $hour, 'minutes' => $minuts, 'secondes' => $secondes];
    return $tab;
}