<?php

include('Application/bdd_connexion.php');

if(!empty($_POST)){
     
     $pseudo = $_POST['member'];
     $mdpVerif = $_POST['mdpVerif'];
     $description = $_POST['description'];
     
     $requete = $pdo->prepare('SELECT * FROM  `member` WHERE  `pseudo` =  ?');
     $requete->execute(array($pseudo));
     $row = $requete->fetch();
     
     //6 derniers caracteres du mot de passe crypté dans la BDD
     $mdpBdd = substr($row['password'], -6);
     
     if($mdpBdd == $mdpVerif){
          $req = $pdo->prepare("UPDATE member SET description =  ? WHERE pseudo = ?");
          $req->execute(array($description,$pseudo));
          echo 'ok';
     } else {
          $message = 'Erreur';
          header('Location: index.php');
          exit;
     }
} else {
     header('Location: index.php');
     exit;
}